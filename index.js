var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');
var session = require('cookie-session');

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(session({ secret: process.env.SESSION_SECRET || 'spiderman' }))

app.get('/', function(req, res) {
  if (!req.session.auth) {
    return res.redirect('/login.html');
  }
  res.sendFile(__dirname + '/notifications.html');
});

app.post('/login', bodyParser.urlencoded({ extended: true }), function(req, res) {
  checkAuth(req.body.username, req.body.password, function(err, success) {
    if (err || !success) {
      req.session.auth = false;
      return res.redirect('/login.html');
    }
    req.session.auth = true;
    res.redirect('/');
  });
});

app.post('/api/v1/notification', function(req, res) {
  // If there's no auth header, send back a 401 UNAUTHORIZED
  if (!req.headers.authorization) {
    return res.status(401).json({
      status: 'error',
      error: 'Missing authorization header.'
    });
  }

  // Parse the authorization header to get the username-password combo
  var header = req.headers.authorization;
  var token = header.split(' ').pop() || ''; // Header is 'Basic <token>' - we're getting the token
  var auth = new Buffer(token, 'base64').toString();
  var parts = auth.split(':'); // Token is 'username:password' - we're splitting it into parts
  var username = parts.length > 0 ? parts[0] : ''; 
  var password = parts.length > 1 ? parts[1] : '';

  checkAuth(username, password, function(err, success) {
    // If auth failed, send back a 401 UNAUTHORIZED
    if (!success) {
      return res.status(401).json({
        status: 'error',
        error: 'Invalid username or password.'
      });
    }

    // Emit a notification event
    io.sockets.emit('notification', {
      package: req.body.package,
      name: req.body.name,
      title: req.body.title,
      titleBig: req.body.titleBig,
      text: req.body.text,
      textBig: req.body.textBig
    });

    // Send a successful response
    res.status(200).json({
      status: 'success'
    });
  });
});

function checkAuth(username, password, callback) {
  var targetUsername = process.env.APP_USERNAME || 'admin';
  var targetPassword = process.env.APP_PASSWORD || 'admin';

  if (username !== targetUsername) {
    return callback(null, false); 
  }
  if (password !== targetPassword) {
    return callback(null, false);
  }
  return callback(null, true);
}

// Start the server
var port = process.env.PORT || 5000;
server.listen(port);
console.log('Listening on port ' + port);
